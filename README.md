# ClickableTestApp

Applicatie voor het testen van DevOps omgevingen.

_Disclaimer!!!_
_Gebruikers zijn vrij de applicatie verder uit te bouwen en te gebruiken._

## Uitleg
Deze applicatie is voor het testen van DevOps omgevingen voor het vak SDS. De app kan zo gebouwd en gedraaid worden maar ook als docker container middels de Dockerfile.
Voor beide manieren zijn kant-en-klare versies beschikbaar middels de [Package Registry](https://gitlab.com/nickrau/clickabletestapp/-/packages) en [Container Registry](https://gitlab.com/nickrau/clickabletestapp/container_registry) van Gitlab.
_Extra uitleg over het gebruik van deze app (bijvoorbeeld app met MYSQL database in docker containers) is te vinden in de [wiki](https://gitlab.com/nickrau/clickabletestapp/-/wikis/home)._

## Opzetten omgeving
Hieronder is de uitleg voor het opzetten van de omgeving. Je kunt de app starten zonder enige parameters mee te geven.
De app maakt zelf een database aan, vult deze met data en start een Tomcat server op waarna het mogelijk is deze via de browser te bereiken.
Via `/h2-console` kom je in een console terecht waar je direct in de database kunt kijken.

### Zonder docker
Gewoon met maven (mvn) bouwen en de JAR file draaien met Java.

_Zorg er wel voor dat je in de map zit van het project_

Build met Maven: `mvn -B package`

_Zorg er wel voor dat je in de map zit waar de JAR file zit "/target"_

Draaien met Java: `java -jar clickabletestapp-[versienummer].jar`

### Met docker
De build doen met Maven (mvn) en d.m.v. de Dockerfile een docker image maken.
_Zorg er wel voor dat je in de map zit van het project_

Build met Maven: `mvn -B package`

_Zorg er wel voor dat je in de map zit waar de Dockerfile zit_

Docker image bouwen: `docker build -t clickabletestapp .`

Als je de dockerimage wil draaien hoef je alleen `docker run -p 8080:8080 clickabletestapp` in te voeren om de docker image te draaien. _Denk eraan dat bij iedere versie je de container op een andere manier moet draaien._

### Geen zin om de docker image te maken of lokaal te bouwen
Geen zin de docker image zelf te bouwen of zelf lokaal te bouwen?
Via de [Container Registry](https://gitlab.com/nickrau/clickabletestapp/container_registry) en [Package Registry](https://gitlab.com/nickrau/clickabletestapp/-/packages) kun je de laatste nieuwe versie van de app ophalen en gebruiken.
Je kunt ook met `docker pull registry.gitlab.com/nickrau/clickabletestapp/clickabletestapp:latest` de laatste nieuwe image ophalen.

## Parameters voor opstarten app
In dit stuk worden alle parameters weergegeven die ingevuld kunnen worden zodra de app gestart wordt.
Dit kan bij java en docker op de volgende manieren:
- Via java `java -jar app.jar --par1=something --par2=something`
- Via docker `docker run -p 8080:8080 nickrau/clickabletestapp -e PAR1="something" -e PAR2="something"`
_Docker stuurt de parameters door naar Java._
  

### Server poort
Java: `--server.port=[POORTNUMMER]`
Docker: `-e SERVER_PORT=[POORTNUMMER]`  
Hiermee kan het poortnummer doorgegeven worden aan de applicatie. (Standaard 8080)

### Database Server MYSQL
Java: `--spring.profile.active=mysql`
Docker: `-e SPRING_PROFILES_ACTIVE=mysql`  
Mogelijkheid om te verbinden met een MYSQL server. Springboot maakt, vult en past de database zelf aan. Dit hoeft de gebruiker niet te doen.  

Middels de onderstaande variabelen kan de gebruiker de connectie verder aanpassen:

__MYSQL USER__  
De gebruikersnaam waarmee de app inlogt. (Default is "user")  
Java: `--MYSQL_USER=[USERNAME]`
Docker: `-e MYSQL_USER=[USERNAME]`  

__MYSQL PASSWORD__  
Het wachtwoord waarme de app inlogt. (Default is "user")  
Java: `--MYSQL_PASSWORD=[WACHTWOORD]`
Docker: `-e MYSQL_PASSWORD=[WACHTWOORD]`  

__MYSQL HOST__  
Adres naar de MYSQL server. (Default is "localhost")  
Java: `--MYSQL_HOST=[HOSTNAME]`
Docker: `-e MYSQL_HOST=[HOSTNAME]`

__MYSQL PORT__  
Poortnummer dat het mogelijk maakt een verbinding te leggen met de MYSQL Server. (Default is 3306)  
Java: `--MYSQL_PORT=[POORTNUMMER]`
Docker: `-e MYSQL_PORT=[POORTNUMMER]`

__MYSQL DATABASE__  
Naam die de database krijgt in MYSQL. (Default is "clickable_default")  
Java: `--MYSQL_DB=[DATABASENAAM]`
Docker: `-e MYSQL_DB=[DATABASENAAM]`  

### Database Server Microsoft SQL Server
Java: `--spring.profile.active=mysql`
Docker: `-e SPRING_PROFILES_ACTIVE=mysql`  
Mogelijkheid om te verbinden met een Microsoft SQL server. Springboot vult en past de database zelf aan. Dit hoeft de gebruiker niet te doen.
Wel moet de gebruiker zelf een database aanmaken in Microsoft SQL Server en ervoor zorgen dat de app de server kan bereiken met een gebruikersnaam en wachtwoord. Hoe wordt uitgelegd in de wiki.

Middels de onderstaande variabelen kan de gebruiker de connectie verder aanpassen:

__MSQL USER__  
De gebruikersnaam waarmee de app inlogt. (Default is "springuser")  
Java: `--MSQL_USER=[USERNAME]`
Docker: `-e MSQL_USER=[USERNAME]`

__MSQL PASSWORD__  
Het wachtwoord waarme de app inlogt. (Default is "Springuser1!")  
Java: `--MSQL_PASSWORD=[WACHTWOORD]`
Docker: `-e MSQL_PASSWORD=[WACHTWOORD]`

__MSQL HOST__  
Adres naar de Microsoft SQL Server. Zorg ervoor dat poort 1433 open is naar de server. (Default is "localhost")  
Java: `--MSQL_HOST=[HOSTNAME]`
Docker: `-e MSQL_HOST=[HOSTNAME]`

__MSQL DATABASE__  
Naam van de database in Microsoft SQL Server. (Default is "clickable_default")  
Java: `--MSQL_DB=[DATABASENAAM]`
Docker: `-e MSQL_DB=[DATABASENAAM]`