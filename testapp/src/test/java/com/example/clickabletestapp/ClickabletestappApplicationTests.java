package com.example.clickabletestapp;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

import java.nio.file.Paths;

@SpringBootTest
class ClickabletestappApplicationTests {
	Logger logger = LoggerFactory.getLogger(ClickabletestappApplicationTests.class);

	@Test
	void myTest() {
		String workingDir = Paths.get("").toAbsolutePath().toString();
		logger.info("Working directory is: "+workingDir);
	}

}
