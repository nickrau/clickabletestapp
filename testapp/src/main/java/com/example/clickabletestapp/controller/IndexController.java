package com.example.clickabletestapp.controller;

import com.example.clickabletestapp.ClickabletestappApplication;
import com.example.clickabletestapp.data.MenuItem;
import com.example.clickabletestapp.data.entity.Course;
import com.example.clickabletestapp.data.entity.Review;
import com.example.clickabletestapp.data.repository.CourseRepository;
import com.example.clickabletestapp.data.repository.TrainerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import static java.util.Map.entry;

@Controller
public class IndexController {

    private List<MenuItem> menu = new ArrayList<>();
    Logger logger = LoggerFactory.getLogger(ClickabletestappApplication.class);

    @Autowired
    CourseRepository courseRepo;
    @Autowired
    TrainerRepository trainerRepo;

    IndexController(){
        menu.add(new MenuItem("Home", "/", ""));
        menu.add(new MenuItem("Over", "/about", ""));
        menu.add(new MenuItem("Cursussen", "/courses", ""));
        menu.add(new MenuItem("Trainers", "/trainers", ""));
        menu.add(new MenuItem("Contact", "/contact", ""));

    }

    private List<Review> getReviews(){
        List<Review> revs = new ArrayList<>();
        File reviewFile = new File("tmpfolder/filename.txt");
        try {
            Scanner scanner = new Scanner(reviewFile);
            while (scanner.hasNextLine()){
                Review newReview = new Review();
                newReview.setReviewText(scanner.nextLine());
                revs.add(newReview);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return revs;
    }


    @RequestMapping("/")
    public String index(Model model) {
        menu.forEach(menuItem -> menuItem.setClasses(""));
        menu.stream()
                .filter(menuItem -> "Home".equals(menuItem.getName()))
                .findAny()
                .orElse(null).setClasses("active");

        model.addAttribute("menu", menu);
        model.addAttribute("review", new Review());
        model.addAttribute("reviewList", getReviews());

        File testFile = new File("tmpfolder/filename.txt");
        try {
            if(testFile.createNewFile())
            {
                logger.info("Created file at "+testFile.getAbsolutePath());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "index";
    }

    @RequestMapping("courses")
    public String courses(Model model){
        menu.forEach(menuItem -> menuItem.setClasses(""));
        menu.stream()
                .filter(menuItem -> "Cursussen".equals(menuItem.getName()))
                .findAny()
                .orElse(null).setClasses("active");

        model.addAttribute("menu", menu);
        model.addAttribute("courses", courseRepo.findAll());
        model.addAttribute("headername", "Courses");
        return "courses";
    }

    @RequestMapping(value="course/{id}", method = RequestMethod.GET)
    public String course(@PathVariable("id") String id, Model model){
        menu.forEach(menuItem -> menuItem.setClasses(""));
        menu.stream()
                .filter(menuItem -> "Cursussen".equals(menuItem.getName()))
                .findAny()
                .orElse(null).setClasses("active");

        Course course = courseRepo.getOne(id);
        model.addAttribute("course", course);
        model.addAttribute("menu", menu);

        return "course-details";
    }
    @RequestMapping("trainers")
    public String trainers(Model model){
        menu.forEach(menuItem -> menuItem.setClasses(""));
        menu.stream()
                .filter(menuItem -> "Trainers".equals(menuItem.getName()))
                .findAny()
                .orElse(null).setClasses("active");

        model.addAttribute("menu", menu);
        model.addAttribute("trainerlist", trainerRepo.findAll());
        return "trainers";
    }

    @RequestMapping("about")
    public String about(Model model){
        menu.forEach(menuItem -> menuItem.setClasses(""));
        menu.stream()
                .filter(menuItem -> "Over".equals(menuItem.getName()))
                .findAny()
                .orElse(null).setClasses("active");

        model.addAttribute("menu", menu);
        return "about";
    }

    @RequestMapping("contact")
    public String contact(Model model){
        menu.forEach(menuItem -> menuItem.setClasses(""));
        menu.stream()
                .filter(menuItem -> "Contact".equals(menuItem.getName()))
                .findAny()
                .orElse(null).setClasses("active");

        model.addAttribute("menu", menu);
        odel.addAttribute("contactForm", new ContactForm());
        return "contact";
    }

    @PostMapping("contact")
    public String contactForm(@ModelAttribute ContactForm contactForm, Model model){
        model.addAttribute("send", false);
        model.addAttribute("error", true);
        model.addAttribute("errorMessage", "Email contact form not working at the moment");
        return "contact";
    }

    @PostMapping("review")
    public String review(@ModelAttribute Review review, Model model){
        File testFile = new File("tmpfolder/filename.txt");
        try {
            if(testFile.createNewFile())
            {
                logger.info("Created file at "+testFile.getAbsolutePath());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            FileWriter writer = new FileWriter("tmpfolder/filename.txt", true);
            writer.write(review.getReviewText()+"\n");
            writer.close();
        } catch (Exception exception){
            String ex = exception.toString();
        }

        model.addAttribute("menu", menu);
        model.addAttribute("review", new Review());
        model.addAttribute("reviewList", getReviews());
        return "index";
    }
}
