package com.example.clickabletestapp.configuration;

import com.example.clickabletestapp.data.entity.Course;
import com.example.clickabletestapp.data.entity.Lessons;
import com.example.clickabletestapp.data.entity.Trainer;
import com.example.clickabletestapp.data.repository.CourseRepository;
import com.example.clickabletestapp.data.repository.TrainerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

@Configuration
public class DataConfiguration {

    @Autowired
    private CourseRepository courseRepo;

    @Autowired
    private TrainerRepository trainerRepo;

    Logger logger = LoggerFactory.getLogger(DataConfiguration.class);

    @EventListener(ApplicationReadyEvent.class)
    void FillWithTestData(){
        // TODO: Vullen met gebruikersdata
    }
}
