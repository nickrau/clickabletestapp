package com.example.clickabletestapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

@SpringBootApplication
@EnableWebMvc
public class ClickabletestappApplication extends WebMvcConfigurerAdapter {

	public static void main(String[] args) {
		Logger logger = LoggerFactory.getLogger(ClickabletestappApplication.class);
		SpringApplication.run(ClickabletestappApplication.class, args);
		String workingDir = Paths.get("").toAbsolutePath().toString();
		logger.info("Working directory is: "+workingDir);
		File testFile = new File(workingDir+"/tmpfolder/"+"filename.txt");
		try {
			if(testFile.createNewFile())
			{
				logger.info("Created file at "+testFile.getAbsolutePath());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		String workingDir = Paths.get("").toAbsolutePath().toString();
		registry.addResourceHandler("/assets/**").addResourceLocations("classpath:/assets/");
		registry.addResourceHandler("/tmpfolder/**").addResourceLocations(workingDir+"/tmpfolder/");
	}


}
