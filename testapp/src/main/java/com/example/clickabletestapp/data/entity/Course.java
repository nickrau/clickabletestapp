package com.example.clickabletestapp.data.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class Course {

    @Id
    private String id;
    private String coursename;
    private String description;
    private String category;
    private Integer price;
    private Integer time;
    private String banner;

    @ElementCollection
    private List<Lessons> lessonsList = new ArrayList<>();

    @ManyToOne
    private Trainer trainer;

    public Course(){
        this.lessonsList = new ArrayList<>();
    }

    @PrePersist
    void generateId() {id = UUID.randomUUID().toString();}
    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    public String getCoursename() { return coursename; }
    public void setCoursename(String coursename) { this.coursename = coursename; }

    public Integer getPrice() { return price; }
    public void setPrice(Integer price) { this.price = price; }

    public List<Lessons> getLessonsList() { return lessonsList; }
    public void setLessonsList(List<Lessons> lessonsList) { this.lessonsList = lessonsList; }
    public void addToLessionList(Lessons lesson){ this.lessonsList.add(lesson); lesson.setLessonOrder(this.lessonsList.size()); }
    public void removeFromLessionList(Lessons lesson){ this.lessonsList.remove(lesson); }

    public Trainer getTrainer() { return trainer; }
    public void setTrainer(Trainer trainer) { this.trainer = trainer; }

    public Integer getTime() { return time; }
    public void setTime(Integer time) { this.time = time; }

    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }

    public String getCategory() { return category; }
    public void setCategory(String category) { this.category = category; }

    public String getBanner() { return banner; }
    public void setBanner(String banner) { this.banner = banner; }
}
