package com.example.clickabletestapp.data.repository;

import com.example.clickabletestapp.data.entity.Trainer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrainerRepository extends JpaRepository<Trainer, String> {
}
