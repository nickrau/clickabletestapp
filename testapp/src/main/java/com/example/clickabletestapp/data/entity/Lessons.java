package com.example.clickabletestapp.data.entity;

import javax.persistence.*;

@Embeddable
public class Lessons {

    private int lessonOrder;
    private String lessonName;
    private String courseVideo;
    @Column(length=1024)
    private String lessonInfo;

    public int getLessonOrder() { return lessonOrder; }
    public void setLessonOrder(int lessionOrder) { this.lessonOrder = lessionOrder; }

    public String getLessonName() { return lessonName; }
    public void setLessonName(String lessonName) { this.lessonName = lessonName; }

    public String getCourseVideo() { return courseVideo; }
    public void setCourseVideo(String courseVideo) { this.courseVideo = courseVideo; }

    public String getLessonInfo() { return lessonInfo; }
    public void setLessonInfo(String lessionInfo) { this.lessonInfo = lessionInfo; }
}
