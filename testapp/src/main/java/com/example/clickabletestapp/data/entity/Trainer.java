package com.example.clickabletestapp.data.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class Trainer {

    @Id
    private String id;
    private String firstName;
    private String lastName;
    private String foto;
    private String description;
    private String about;

    @OneToMany
    private List<Course> courseList;

    public Trainer(){
        this.courseList = new ArrayList<>();
    }

    @PrePersist
    void generateId() {id = UUID.randomUUID().toString();}
    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    public String getFirstName() { return firstName; }
    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }
    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getFoto() { return foto; }
    public void setFoto(String foto) { this.foto = foto; }

    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }

    public String getAbout() { return about; }
    public void setAbout(String about) { this.about = about; }

    public List<Course> getCourseList() { return courseList; }
    public void setCourseList(List<Course> courseList) { this.courseList = courseList; }
    public void addToCourseList(Course course){ this.courseList.add(course); }
    public void removeFromCourseList(Course course) { this.courseList.remove(course); }
}
