package com.example.clickabletestapp.data.repository;

import com.example.clickabletestapp.data.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, String> {
}
