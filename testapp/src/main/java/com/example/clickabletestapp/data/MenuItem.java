package com.example.clickabletestapp.data;

public class MenuItem {
    private String name;
    private String source;
    private String classes;

    public MenuItem(String name, String source, String classes){
        this.classes = classes;
        this.source = source;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getClasses() {
        return classes;
    }

    public void setClasses(String classes) {
        this.classes = classes;
    }

    public void addToClasses(String className){
        this.classes = this.classes + " " + className;
    }

    public void removeFromClasses(String className){
        this.classes.replace(" "+className, "");
    }
}
