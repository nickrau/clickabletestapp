from python.datastructure.clickable_datastructure import *
import uuid
import os

insert_course_sql="""INSERT INTO COURSE VALUES ('##ID##', '##BANNER##', '##CATEGORY##', '##COURSENAME##', '##DESCRIPTION##', '##PRICE##', '##TIME##', '##TRAINERID##');

"""
insert_trainer_sql="""INSERT INTO TRAINER VALUES ('##ID##', '##ABOUT##', '##DESCRIPTION##', '##FIRSTNAME##', '##PHOTO##', '##LASTNAME##');

"""
insert_lesson_sql="""INSERT INTO COURSE_LESSONS_LIST VALUES ('##COURSEID##', '##COURSEVIDEO##', '##LESSONINFO##', '##LESSONORDER##', '##LESSONNAME##');

"""
insert_course_trainer_sql="""INSERT INTO TRAINER_COURSE_LIST VALUES ('##TRAINERID##', '##COURSELISTID##');

"""

def setToSQL(trainers, courses):
    with open("../sql/courses/courses-h2.sql", 'a') as coursefile:

        for trainer in trainers:
            # TODO: Put trainers to SQL
            insert_trainer = insert_trainer_sql.replace("##ID##",trainer.id)
            insert_trainer = insert_trainer.replace("##ABOUT##", trainer.about)
            insert_trainer = insert_trainer.replace("##DESCRIPTION##", trainer.description)
            insert_trainer = insert_trainer.replace("##FIRSTNAME##", trainer.firstname)
            insert_trainer = insert_trainer.replace("##PHOTO##", trainer.photo)
            insert_trainer = insert_trainer.replace("##LASTNAME##", trainer.lastname)
            coursefile.write(insert_trainer)

        for course in courses:
            # TODO: Put courses to SQL
            insert_course = insert_course_sql.replace("##ID##", course.id)
            insert_course = insert_course.replace("##BANNER##", course.banner)
            insert_course = insert_course.replace("##CATEGORY##", course.category)
            insert_course = insert_course.replace("##COURSENAME##", course.coursename)
            insert_course = insert_course.replace("##DESCRIPTION##", course.description)
            insert_course = insert_course.replace("##PRICE##", course.price)
            insert_course = insert_course.replace("##TIME##", course.time)
            insert_course = insert_course.replace("##TRAINERID##", course.trainer.id)
            coursefile.write(insert_course)

        for course in courses:
            for lesson in course.lessons:
                insert_lesson = insert_lesson_sql.replace("##COURSEID##", course.id)
                insert_lesson = insert_lesson.replace("##COURSEVIDEO##", lesson.cousevideo)
                insert_lesson = insert_lesson.replace("##LESSONINFO##", lesson.lessoninfo)
                insert_lesson = insert_lesson.replace("##LESSONORDER##", lesson.order)
                insert_lesson = insert_lesson.replace("##LESSONNAME##", lesson.lessonname)
                coursefile.write(insert_lesson)
            insert_course_trainer = insert_course_trainer_sql.replace("##TRAINERID##", course.trainer.id)
            insert_course_trainer = insert_course_trainer.replace("##COURSELISTID##", course.id)
            coursefile.write(insert_course_trainer)

def start():
    walter = Trainer(str(uuid.uuid4()), "Walter", "White", "trainer-1.jpg", "Na jaren in de IT gewerkt te hebben vond Walter dat het tijd was om zijn kennis te delen.", "IT-Docent op de universiteit")
    sarah = Trainer(str(uuid.uuid4()), "Sarah", "Jhinson", "trainer-2.jpg", "Nog steeds erg actief bij het grote Google. Maar daarnaast vindt ze het leuk om mensen te helpen die er niks van snappen.", "Programmeur bij Google")
    william = Trainer(str(uuid.uuid4()), "William", "Anderson", "trainer-3.jpg", "William kent alles van Sociale Media. Hij heeft zelfs bij Facebook gewerkt. Dus hij weet als geen ander hoe je met sociale media omgaat.", "Social Media Expert")
    trainers = [walter, sarah, william]

    windowsCourse = Course(str(uuid.uuid4()), "Omgaan met Windows", "Windows", "course-1.jpg", "Leer in deze cursus hoe je omgaat met Windows. Van inloggen tot aan het installeren van programma's.", "5", "250", walter)
    googleCourse = Course(str(uuid.uuid4()), "Zoeken met Google", "Google", "course-2.jpg", "Hoe vind je wat je zoekt met Google. Leer in deze cursus de fijne kneepjes van het gebruiken van deze zoekmachine.", "5", "250", sarah)
    facebookCourse = Course(str(uuid.uuid4()), "Omgaan met Facebook", "Facebook", "course-3.jpg", "Of je nou foto's deelt van je kleinkinderen, reageert op berichten van anderen of je profielfoto verandert. In deze cursus leer je hoe je omgaat met Facebook.", "2", "180", william)

    windowslessons = []
    windowslessons.append(Lesson("1", "video.mp4", "Inloggen in Windows", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."))
    windowslessons.append(Lesson("2", "video.mp4", "Achtergrond veranderen", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."))
    windowslessons.append(Lesson("3", "video.mp4", "Verkenner", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."))
    windowslessons.append(Lesson("4", "video.mp4", "Kopieren/plakken", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."))
    windowslessons.append(Lesson("5", "video.mp4", "Programma's installeren", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."))
    windowsCourse.lessons = windowslessons

    googleLessons = []
    googleLessons.append(Lesson("1", "video.mp4", "Zoekopdracht", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."))
    googleLessons.append(Lesson("2", "video.mp4", "Speciale tekens", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."))
    googleLessons.append(Lesson("3", "video.mp4", "Snelle antwoorden", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."))
    googleLessons.append(Lesson("4", "video.mp4", "Geadvanceerd zoeken", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."))
    googleLessons.append(Lesson("5", "video.mp4", "Met afbeeldingen zoeken", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."))
    googleCourse.lessons = googleLessons

    facebookLessons = []
    facebookLessons.append(Lesson("1", "video.mp4", "Account aanmaken", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."))
    facebookLessons.append(Lesson("2", "video.mp4", "Post schrijven", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."))
    facebookLessons.append(Lesson("3", "video.mp4", "Reageren", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."))
    facebookLessons.append(Lesson("4", "video.mp4", "Post feed", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."))
    facebookLessons.append(Lesson("5", "video.mp4", "Vrienden toevoegen", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."))
    facebookCourse.lessons = facebookLessons

    courses = [windowsCourse,googleCourse,facebookCourse]

    print("##### TRAINERS #####")
    for trainer in trainers:
        print(trainer.firstname + " " + trainer.lastname + " " + trainer.id + " " + trainer.description)
    print()

    print("##### COURSES #####")
    for course in courses:
        print(course.id + " " + course.coursename + " " + course.description)
        print("##### LESSONS OF COURSE: " + course.coursename + " #####")
        for lesson in course.lessons:
            print(lesson.order + " " + lesson.lessonname)
        print()

    setToSQL(trainers, courses)
if __name__ == "__main__":
    start()