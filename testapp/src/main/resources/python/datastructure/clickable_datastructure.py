class Trainer:
    def __init__(self, id, firstname, lastname, photo, description, about):
        self.id = id
        self.firstname = firstname
        self.lastname = lastname
        self.photo = photo
        self.description = description
        self.about = about


class Course:
    def __init__(self, id, coursename, category, banner, description, time, price, trainer, lessons = []):
        self.id = id
        self.coursename = coursename
        self.category = category
        self.banner = banner
        self.description = description
        self.time = time
        self.price = price
        self.trainer = trainer
        self.lessons = lessons

    def addToLessons(self, lesson):
        self.lessons.append(lesson)

    def returnOrderLesson(self, lesson):
        return lesson.order

    def sortLessons(self):
        self.lessons.sort(key=self.returnOrderLesson)

class Lesson:

    def __init__(self, order, cousevideo, lessonname, lessoninfo):
        self.order = order
        self.cousevideo = cousevideo
        self.lessonname = lessonname
        self.lessoninfo = lessoninfo